from kivy.lang import Builder
from kivymd.app import MDApp
from kivymd.uix.bottomsheet import MDBottomSheet
from kivymd.uix.list import OneLineListItem
from db_lib import create_data
from db_lib import get_types
from db_lib import get_food
import sys

class Main(MDApp):
    def build(self):
        return Builder.load_file('main.kv')

    def poblate_list(self,list_widget,data, type):
        def go_list(event, food_name=None,id_food=None):
            self.root.ids.screen_manager.current = "screen_food"
            self.root.ids.screen_food_title.text = food_name
            self.poblate_list(self.root.ids.foodList,get_food(id_food-1),1)

        print(self)
        print(list_widget)
        print(data)
        while(len(list_widget.children) > 0):
            children = list_widget.children
            if children:
                for i in children:
                    list_widget.remove_widget(i)

        if data == None or data == []:
            sys.exit()
        for i in data:
            item = OneLineListItem(text=i[1])
            if type == 0:
                item.bind(on_press=lambda event, id_food=i[0], food_name=i[1]:go_list(event,food_name,id_food))
            list_widget.add_widget(item)

    def on_start(self):
        #add food
        def my_func(event):
            self.root.ids.screen_manager.current = "screen_type"
        list_widget = self.root.ids.typeList
        types = get_types()
        self.poblate_list(list_widget,types,0)
        self.root.ids.screen_food_return.bind(on_press= my_func )


if __name__ == "__main__":
    #create_data()
    Main().run()
