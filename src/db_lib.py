import sqlite3
import os
import csv

def read_csv(_file, _tipo,_data):
    with open(_file) as csv_file:
        csv_reader = csv.reader(csv_file,delimiter=',')
        for i in csv_reader:
            tmpDict = (i[0],i[1],i[2], _tipo)
            _data.append(tmpDict)
    return True

def create_connection(file):
    #check if db file exists
    if os.path.exists(file):
        print(file,"exists")
        return True

    #create database
    try:
        conn = sqlite3.connect(file)
        print(file,"created")
        return True
    except Exception as e:
        print("Error creating",file)
        print(e)
        return False
def get_con(file):
    return sqlite3.connect(file)

def execute_query(file, query,query_param = None):
    con = get_con(file)
    cur = con.cursor()
    print(25,query)
    if query_param:
        return cur.execute(query, query_param)
    else:
        return cur.execute(query)

def get_types():
    query = "select * from tipo"
    res = execute_query("database.db",query)
    return res.fetchall()

def get_food(id_type):
    query = "select * from alimento where tipo = ?"
    res = execute_query("database.db",query, (id_type,))
    return res.fetchall()


def create_data():
    data = []
    read_csv("verduras.csv", 0 ,data)
    read_csv("frutas.csv", 1 ,data)
    read_csv("cereales_sin_grasa.csv", 2 ,data)
    read_csv("cereales_con_grasa.csv", 3 ,data)
    read_csv("leguminosas.csv", 4 ,data)
    read_csv("animal_muy_bajo_grasa.csv", 5 ,data)
    read_csv("animal_bajo_grasa.csv", 6 ,data)
    read_csv("animal_moderado_grasa.csv", 7 ,data)
    read_csv("animal_alto_grasa.csv", 8 ,data)
    read_csv("leche_descremada.csv", 9 ,data)
    read_csv("leche_semi_descremada.csv", 10 ,data)
    read_csv("leche_entera.csv", 11 ,data)
    read_csv("leche_con_azucar.csv", 12 ,data)
    read_csv("aceites_y_grasas.csv", 13 ,data)
    read_csv("aceites_y_grasa_protenia.csv", 14 ,data)
    read_csv("azucares_sin_grasa.csv", 15 ,data)
    read_csv("azucares_con_grasa.csv", 16 ,data)
    db_file = "database.db"
    create_connection( db_file )
    tabla = ""
    tipos = ('verduras','frutas','cereales sin grasa','cereales con grasa','leguminosas',
            'Animal muy bajo grasa', 'Animal bajo grasa','Animal moderado grasa',
            'Animal alto grasa','Leche descremada','Leche semi descremada','leche entera',
            'Leche con azucar','Aceite y grasas','Aceites y grasas proteinas','Azucares sin grasa',
            'Azucares con grasa')
    print("tipos",tipos)
    for i in range(2):
        if i == 1:
            query = '''
                CREATE TABLE alimento(
                    id integer primary key autoincrement,
                    nombre varchar(100),
                    cantidad varchar(50),
                    cantidad_g varchar(20),
                    tipo int
                )
            '''
            tabla = 'alimento'
        else:
            query = '''
                CREATE TABLE tipo(
                    id integer primary key autoincrement,
                    nombre varchar(100)
                )
            '''
            tabla = 'tipo'

        con = get_con(db_file)
        cur = con.cursor()
        tabla_query = "SELECT name FROM sqlite_master WHERE name = '" + tabla + "'"
        print(tabla_query)
        res = cur.execute(tabla_query)
        if res.fetchone() == None:
            execute_query(db_file,query)
        else:
            print("Table {tabla} exists")
        res = cur.execute("select * from " + tabla)
        if res.fetchall() == []:
            if i == 1:
                cur.executemany("INSERT INTO alimento(nombre,cantidad,cantidad_g,tipo) VALUES(?,?,?,?)",data)
                con.commit()
            else:
                for i in tipos:
                    print(i)
                    cur.execute("INSERT INTO tipo(nombre) VALUES(?)",(i,))
                    con.commit()
            print("Data was inserted")
        else:
            print("Data exists")
